package com.haizhi.test_springsecurity.repository;

import com.haizhi.test_springsecurity.model.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SysUserRepository extends JpaRepository<SysUser, Long> {
    SysUser findByUsername(String username);
}
